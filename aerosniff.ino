///@file aerosniff.ino
// ======================================================================
// Compile-time constants
// ======================================================================

// Timer #defines and globals

/// Firmware identifier - CHANGE THIS TO SUIT YOUR ORGANIZATION
#define VERSION_ID "20181109_1214"

/// Vendor contact information - CHANGE THIS TO YOUR CONTACT INFO
#define CONTACT_INFO "Bob Apthorpe <bob.apthorpe@acorvid.com>"

/// Milliseconds per second
#define SEC_IN_MILLIS 1000

/// Milliseconds per second
#define SEC_IN_MICROS 1000000

/// Node32s dev board/ESP32 clock prescaler
#define PRESCALER0 80

/// Define baud rate for serial console
#define CONSOLE_BAUD 115200

// ======================================================================
// Standard C/C++ libraries
// ======================================================================
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <sys/time.h>

// ======================================================================
// Arduino-ESP32 libraries; see https://github.com/espressif/arduino-esp32
// ======================================================================
#include "FS.h"

// ======================================================================
// Hardware defines
// ======================================================================

/// If defined, an SD card reader is present
#define HAS_SDCARD
#ifdef HAS_SDCARD

// SD card filesystem
// Arduino-ESP32 libraries; see https://github.com/espressif/arduino-esp32
# include "SD.h"
#endif // ifdef HAS_SDCARD

/// If defined, the ESP32 flash file system is available via SPIFFS
#define HAS_SPIFFS
#ifdef HAS_SPIFFS

// SPIFFS (ESP32 flash) filesystem
// Arduino-ESP32 libraries; see https://github.com/espressif/arduino-esp32
# include "SPIFFS.h"
#endif // ifdef HAS_SPIFFS

/// If defined, a UART-compatible GPS receiver is present
#define HAS_GPS
#ifdef HAS_GPS

/// Define baud rate for GPS UART
# define GPS_BAUD 9600

// Used but has been included/defined previously by the ESP32-Arduino
// framework; see GPS setup
// #include "HardwareSerial.h"

// TinyGPS++ driver; see https://github.com/mikalhart/TinyGPSPlus
# include <TinyGPS++.h>

#endif // ifdef HAS_GPS

/// If defined, a PM1 low pulse occupancy (LPO) dust sensor is present
#define HAS_PM1

/// If defined, a PM2.5 low pulse occupancy (LPO) dust sensor is present
#define HAS_PM2_5

/// If defined, a BME280 barometric pressure, temperature, and relative
//  humidity sensor is present
#define HAS_BME280
#ifdef HAS_BME280

// Adafruit BME driver; see https://github.com/Takatsuki0204/BME280-I2C-ESP32
# include "Adafruit_BME280.h"
#endif // ifdef HAS_BME280

/// If defined, an MQ-2 combustible gas sensor is present
#define HAS_MQ2

// ======================================================================
// Provisioned (device-specific) data
// TODO: Sort out how to provision these user-configurable data. SD card?
// These parameters need to be set on a per-device and per-project basis
// and should be easily configurable in bulk. The use case is a project
// with 1000 sensors which need a unique device ID, initial time
// settings for an RTC, and warmup time and sampling duration set per
// vendor or investigator requirements.
// ======================================================================

class AerosniffConfig {
public:
// TODO: Migrate attributes to private and have them set by provisioner, accessor, or default
// private:

  /// Unique device name. Please limit to [A-Za-z0-9] and less than 16
  //  characters
  std::string device_id             { "P001" };
  
  /// Serial (UNIX seconds-from-epoch) time. Time should be set during
  //  provisioning and updated via NTP or GPS. Generate with `date +"%s"`
  //  on UNIX.
  time_t utctime_frozen             { 1541787295 };
  
  /// Nominal sampling interval duration, in milliseconds. Constant
  uint64_t nominal_sample_time      { 30 };
  
  /// Dust sensor warmup time. Default is 3 minutes.
  // const uint32_t sensor_warmup_time   {3 * 60 };
  // Use 30 seconds for testing
  uint32_t sensor_warmup_time       { 30 };
  
  // DEBUG: Use 5 seconds for testing
  // uint32_t sensor_warmup_time         { 5 };
  
  /// Seconds to wait for peripherals to quiesce after boot
  uint32_t peripheral_boot_delay    { 10 };
  
  /// Seconds between filesystem listings
  uint32_t filesystem_list_interval { 3600 };
  
  /// Seconds between GPS time/location checks
  uint32_t gps_check_interval       { 3600 };

// public:
  /**
   * @brief Get accessor for nominal sample time in milliseconds
   * @returns Nominal sample time in milliseconds
   */
  uint64_t get_nominal_sample_millis() {
    return nominal_sample_time * SEC_IN_MILLIS;
  }

  /**
   * @brief Get accessor for sensor warmup time in milliseconds
   * @returns Sensor warmup time in milliseconds
   */
  uint32_t get_sensor_warmup_millis() {
    return sensor_warmup_time * SEC_IN_MILLIS;
  }

  /**
   * @brief Get accessor for peripheral boot delay in milliseconds
   * @returns Peripheral boot delay in milliseconds
   */
  uint32_t get_peripheral_boot_millis() {
    return peripheral_boot_delay * SEC_IN_MILLIS;
  }

  /**
   * @brief Get accessor for filesystem list interval in milliseconds
   * @returns Filesystem list interval in milliseconds
   */
  uint32_t get_filesystem_list_millis() {
    return filesystem_list_interval * SEC_IN_MILLIS;
  }

  /**
   * @brief Get accessor for GPS check interval in milliseconds
   * @returns GPS check interval in milliseconds
   */
  uint32_t get_gps_check_millis() {
    return gps_check_interval * SEC_IN_MILLIS;
  }

};

AerosniffConfig acfg;

// ======================================================================
// Function prototypes
// ======================================================================

void        checksum_logstream(std::stringstream& ss);
std::string utc_timestamp(const time_t serial_time);

// ======================================================================
// Generic function definitions
// ======================================================================

// ======================================================================
// Pin definitions
// ======================================================================

// Note: The Node32S onboard LED pin number is internally defined as LED_BUILTIN

#ifdef HAS_PM1

/// Dust sensor digital pin D0 (PM1) is connected to ADC pin 4
//  (GPIO pin 32) which connects to Node32s pin P32
const uint8_t PM1Pin { A4 };
#endif // ifdef HAS_PM1

#ifdef HAS_PM2_5

/// Dust sensor digital pin D1 (PM2.5) is connected to ADC pin 5
//  (GPIO pin 33) which connects to Node32s pin P33
const uint8_t PM2_5Pin { A5 };
#endif // ifdef HAS_PM2_5

#ifdef HAS_MQ2

/// MQ-2 gas sensor analog pin A0 connects to ADC pin 6 which connects
//  to Node32s pin P34
const uint8_t gasA0 { A6 };

/// MQ-2 gas sensor digital pin D0 connects to ADC pin 7 which connects
//  to Node32s pin P35
const uint8_t gasD0 { A7 };
#endif // ifdef HAS_MQ2

#ifdef HAS_BME280

// BME280 setup
// Set I2C address and pin numbers for SDA and SCL
// If the sensor does not work, try the 0x77 address as well
// Default: #define BME280_ADDRESS 0x76
// #define BME280_ADDRESS 0x77
// #define I2C_SDA 27
// #define I2C_SCL 26

/// Temperature/pressure/humidity sensor I2C SDA pin connects to
//  GPIO pin 27 which connects to Node32s pin P27
const uint8_t PT_SDA { 27 };

/// Temperature/pressure/humidity sensor I2C SCL pin connects to
//  GPIO pin 26 which connects to Node32s pin P26
const uint8_t PT_SCL { 26 };
#endif // ifdef HAS_BME280

#ifdef HAS_GPS

// Set GPS to use serial port 1
// Note: Serial1 and Serial2 are already defined in
// cores/esp32/HardwareSerial.cpp
// HardwareSerial Serial1(1);

/// GPS digital pin TX connects to GPIO pin 16 (set to RX) which
//  connects to Node32s pin P16
const uint8_t gpsTXmcuRX { 16 };

/// GPS digital pin RX connects to GPIO pin 17 (set to TX) which
//  connects to Node32s pin P17
const uint8_t gpsRXmcuTX { 17 };
#endif // ifdef HAS_GPS

// ======================================================================
// Temperature, pressure, and humidity sensor definitions
// ======================================================================

#ifdef HAS_BME280

// TODO: Migrate this into AmbientAir somehow
/// BME280 barometric pressure, temperature, and relative humidity
//  sensor object
Adafruit_BME280 bme280(PT_SDA, PT_SCL);

class AmbientAir {
public:

  /// Temperature/pressure/humidity sensor I2C SDA pin connects to
  //  GPIO pin 27 which connects to Node32s pin P27
  uint8_t SDAPin { PT_SDA };

  /// Temperature/pressure/humidity sensor I2C SCL pin connects to
  //  GPIO pin 26 which connects to Node32s pin P26
  uint8_t SCLPin { PT_SCL };

  // Ambient air temperature, Celsius
  float temperature { 0.0 };

  // Relative humidity, (units?)
  float humidity    { 0.0 };

  // Barometric pressure, kPa
  float pressure    { 0.0 };

  /**
     @brief Constructor

     @param SCL_pin Sensor I2C SCL pin
     @param SDA_pin Sensor I2C SDA pin
   */
  AmbientAir(uint8_t SDA_pin = PT_SDA, uint8_t SCL_pin = PT_SCL) {
    SDAPin = SDA_pin;
    SCLPin = SCL_pin;

    clear_data();
  }

  /**
     @brief Clear temperature, pressure, and humidity data
   */
  void clear_data() {
    temperature = 0.0;
    humidity    = 0.0;
    pressure    = 0.0;
  }

  /**
     @brief Read temperature, pressure, and humidity data
   */
  void read_sensor() {
    temperature = bme280.readTemperature();
    humidity    = bme280.readHumidity();
    pressure    = bme280.readPressure();
  }

  /**
     @brief Create pressure/temperature sensor log entry and append it to a
     logstream

     @param logstream std::stringstream object for accumulating log entries
   */
  void add_to_logstream(std::stringstream& logstream) {
    auto t  = std::time(nullptr);
    auto tm = *std::gmtime(&t);

    std::stringstream ssnew;

    ssnew << "L=\"PT\"";
    ssnew << ", ID=\"" << acfg.device_id << "\"";
    ssnew << ", UTC=\"" << std::put_time(&tm, "%Y%m%dT%H%M%SZ") << "\"";
    ssnew << ", PATM=" << std::setprecision(7) << pressure;
    ssnew << ", TATM=" << std::setprecision(4) << temperature;
    ssnew << ", RH="   << std::setprecision(4) << humidity;
    checksum_logstream(ssnew);
    ssnew << std::endl;

    logstream << ssnew.str().c_str();
  }
};

AmbientAir localAir(PT_SDA, PT_SCL);
#endif // ifdef HAS_BME280

// ======================================================================
// GPS definitions
// ======================================================================

#ifdef HAS_GPS

/**
   @brief GPS position class
 */
class GPSPosition {
private:

  /// Clock update flag; indicates clock has been updated since last read.
  bool _clockUpdate { false };

public:

  /// GPS UART TX pin corresponding to RX pin on microcontroller
  uint8_t TXPin;

  /// GPS UART RX pin corresponding to TX pin on microcontroller
  uint8_t RXPin;

  /// The TinyGPS++ object
  TinyGPSPlus gps;

  /// Serial time when clock was last set from GPS (seconds since UNIX epoch)
  time_t lastGPSClockSet { 0 };

  /// GPS update interval, seconds
  uint32_t updateInterval { 3600 };

  /// Latitude
  double lat { 0.0 };

  /// Longitude
  double lng { 0.0 };

  /// Altitude, in meters. Note: Austin is 187.7 m above mean sea level
  double alt { 0.0 };

  /**
     @brief Constructor

     @param check_interval Seconds between GPS time/location checks
   */
  GPSPosition(uint32_t check_interval = 3600) {
    if (check_interval > 30) {
      updateInterval = check_interval;
    }
    clear_position_data();
  }

  /**
     @brief Set GPS TX/RX pins and initialize serial/UART connection.
     Note that GPS uses Serial1 by default.

     @param TX GPS UART pin corresponding to RX pin on microcontroller
     @param RX GPS UART pin corresponding to TX pin on microcontroller
   */
  void initialize_receiver(const uint8_t TX, const uint8_t RX) {
    TXPin = TX;
    RXPin = RX;

    Serial1.begin(GPS_BAUD, SERIAL_8N1, TXPin, RXPin);
  }

  /**
     @brief Create a GPS entry for the log and add it to a logstream

     @param logstream std::stringstream object. Read-write
   */
  void add_to_logstream(std::stringstream& logstream) {
    std::string ststamp = utc_timestamp(std::time(nullptr));

    _clockUpdate = false;

    logstream << "L=\"GPS\"";
    logstream << ", ID=\"" << acfg.device_id << "\"";
    logstream << ", UTC=\"" << ststamp << "\"";
    logstream << ", LAT=" << std::setprecision(8) << lat << std::defaultfloat;
    logstream << ", LNG=" << std::setprecision(8) << lng << std::defaultfloat;
    logstream << ", ALT=" << std::setprecision(6) << alt << std::defaultfloat;

    checksum_logstream(logstream);
    logstream << std::endl;
  }

  /**
     @brief Clear GPS postion data
   */
  void clear_position_data() {
    lat = 0.0;
    lng = 0.0;
    alt = 0.0;
  }

  /**
     @brief Set time and location from GPS
   */
  void adjust_spacetime() {
    auto rightNow = std::time(nullptr);

    if (abs(rightNow - lastGPSClockSet) > updateInterval) {
      std::stringstream ssmsg;

      _clockUpdate = false;
      double new_alt { 0.0 };
      bool   altReset { false };
      bool   locReset { false };

      while (!(_clockUpdate && altReset && locReset)) {
        while (Serial1.available() > 0) {
          if (gps.encode(Serial1.read())) {
            if (gps.altitude.isUpdated() && !altReset) {
              new_alt = gps.altitude.meters();

              if (new_alt != 0.0) {
                alt      = new_alt;
                altReset = true;
              }
            }

            if (gps.location.isUpdated() && !locReset) {
              lat      = gps.location.lat();
              lng      = gps.location.lng();
              locReset = true;
            }

            if (gps.date.isValid() && gps.date.isUpdated()
                && gps.time.isValid() && gps.time.isUpdated()
                && !_clockUpdate) {
              struct tm tmcompose;

              tmcompose.tm_sec  = gps.time.second();
              tmcompose.tm_min  = gps.time.minute();
              tmcompose.tm_hour = gps.time.hour();
              tmcompose.tm_mday = gps.date.day();
              tmcompose.tm_mon  = gps.date.month() - 1;
              tmcompose.tm_year = gps.date.year() - 1900;

              struct timeval tvGPS;

              // Set time
              time_t right_now = mktime(&tmcompose);

              if (right_now >= acfg.utctime_frozen) {
                tvGPS.tv_sec  = right_now;
                tvGPS.tv_usec = 0;

                ssmsg.str(std::string());
                ssmsg << "INFO: Setting clock to GPS serial time "
                      << tvGPS.tv_sec << std::endl;
                Serial.print(ssmsg.str().c_str());

                settimeofday(&tvGPS, NULL);
                _clockUpdate = true;

                lastGPSClockSet = std::time(nullptr);

                ssmsg.str(std::string());
                ssmsg << "INFO: System time and date set from GPS and updated to "
                      << utc_timestamp(lastGPSClockSet) << std::endl;
                Serial.print(ssmsg.str().c_str());

                ssmsg.str(std::string());
                ssmsg << "INFO: Latitude is " << lat
                      << ", Longitude is " << lng
                      << ", Altitude is " << alt << " m" << std::endl;
                Serial.print(ssmsg.str().c_str());
              }

              //              } else {
              //                Serial.println("DEBUG: GPS date not valid.");
            }
          }
        }
      }
    }
  }

  /**
     @brief Accessor which show whether the clock has been updated
     since the last write to logs.
   */
  bool updated() {
    return _clockUpdate;
  }
};

/// GPS position object
GPSPosition GPSPos;

#endif // ifdef HAS_GPS

// ======================================================================
// Mu(te?)xes for interrupt handling
// ======================================================================

/// Mu(te?)x for PM1
portMUX_TYPE PM1Mux = portMUX_INITIALIZER_UNLOCKED;

/// Mu(te?)x for PM2.5
portMUX_TYPE PM2_5Mux = portMUX_INITIALIZER_UNLOCKED;

// ======================================================================
// Sampling and detection intervals and bounds
// Note the use of 'volatile' to prevent inadvertent deletion via
// compiler optimization
// TODO: Refactor into sampling and dust sensor objects
// ======================================================================

/// Device start time measured at end of setup(), in milliseconds.
uint32_t deviceStartTime { 0 };

/// State of dust sampling interval - either ACTIVE (sampling) or
//  INACTIVE (not sampling).
enum class DustSamplingState {
  INACTIVE,
  ACTIVE
};

/**
   @brief Dust sampling interval class
 */
class DustSamplingInterval {
public:

  /// Current dust sampling state
  volatile DustSamplingState currState {
    DustSamplingState::INACTIVE
  };

  /// Previous dust sampling state
  volatile DustSamplingState prevState {
    DustSamplingState::INACTIVE
  };

  /// Nominal end time of sampling interval in milliseconds. Volatile.
  volatile uint32_t nominalEndMillis { 0 };

  /// Start time of sampling interval in microseconds. Volatile.
  volatile uint64_t startMicros { 0 };

  /// End time of sampling interval in microseconds. Volatile.
  volatile uint64_t endMicros { 0 };

  /// Start time of sampling interval in milliseconds. Volatile.
  volatile uint32_t startMillis { 0 };

  /// End time of sampling interval in milliseconds. Volatile.
  volatile uint32_t endMillis { 0 };

  /**
     @brief Constructor
   */
  DustSamplingInterval() {
    currState = DustSamplingState::INACTIVE;
    reset_sampling_interval();
  }

  /**
     @brief Save current state as previous state
   */
  void update_prev_state() {
    prevState = currState;
  }

  /**
     @brief Change current state and save history

     @param newState DustSamplingState to change the current state to.
   */
  void change_state_to(const DustSamplingState newState) {
    update_prev_state();
    currState = newState;
  }

  /**
     @brief Initialize variables associated with dust sampling
   */
  void reset_sampling_interval() {
    //  Serial.println("INFO: reset_sampling_interval()");

    nominalEndMillis = 0;
    endMillis        = 0;
    startMillis      = 0;
    endMicros        = 0;
    startMicros      = 0;

    change_state_to(DustSamplingState::INACTIVE);
  }
};

/// Dust sampling interval object
DustSamplingInterval sampleInterval;

/// Dust sensor line states. Either NO_DATA (prior to first detected
//  transition), DETECTING (after a transition to low), or
//  NOT_DETECTING (after a transition to high)
enum class DustSensorState {
  STOPPED,
  NO_DATA,
  DETECTING,
  NOT_DETECTING
};

/**
   @brief Dust sensor line state and timing/LPO class
 */
class DustLPO {
  using funcPtr = void (*)(void);

public:

  /// Text ID of dust sensor, e.g. "PM1", "PM2.5". Read-only.
  std::string dustTag;

  /// Pin assigned to sensor
  uint8_t dustPin;

  /// Dust sensor multiplexer(?), mutex(?)
  portMUX_TYPE dustMux;

  /// Current dust sampling state. Volatile.
  volatile DustSensorState currState {
    DustSensorState::STOPPED
  };

  /// Previous dust sampling state. Volatile.
  volatile DustSensorState prevState {
    DustSensorState::STOPPED
  };

  /// Start time of last transition to low on dust sensor data line,
  //  in microseconds. See onDustPinChange().
  volatile uint64_t detectionStartTime { 0 };

  /// Total low occupancy duration of dust data line,
  //  in microseconds; see onDustPinChange(). Volatile.
  volatile uint64_t totalDetectionDuration { 0 };

  /// Number of state transitions during sampling interval;
  //  see onDustPinChange(). Volatile.
  volatile uint32_t nTransitions { 0 };

  /// Flag indicating missed dust sensor data interrupts; see
  //  onDustPinChange(). Volatile.
  volatile bool suspectSample { false };

  /// ISR ID - historical and somewhet deprecated;
  //  see onDustPinChange(). Volatile.
  volatile uint16_t suspectISR { 0 };

  /// Boolean flag to indicate this sensor's readings during the current
  //  sampling interval may be inaccurate due to missing sensor line
  //  state transitions. See onDustPinChange()
  volatile DustSensorState suspectState;

  /// Time at which the last suspect transition was detected,
  //  milliseconds since device start. Volatile.
  volatile uint16_t suspectMillis;

  /// Count of duplicate dust detected transitions;
  //  see onDustPinChange(). Volatile.
  volatile uint16_t nDuplicateDust { 0 };

  /// Count of duplicate sensor clear transitions;
  //  see onDustPinChange(). Volatile.
  volatile uint16_t nDuplicateClear { 0 };

  /// Low pulse occupancy ratio, in percent.
  //  100 * totalDetectionDuration / (sampleInterval.endMicros -
  // sampleInterval.startMicros)
  double dustRatio { 0.0 };

  /// Aerosol concentration, in particles above aerodynamic diameter D per liter
  double concentration { 0.0 };

  /**
     @brief Constructor; sets tag and dustPin attributes

     @param tag Text ID of dust sensor, e.g. "PM1", "PM2.5". Read-only.
     @param pin Pin number of dust sensor line. Read-only.
   */
  DustLPO(const std::string tag = "PM1", const uint8_t pin = A4) {
    dustTag = tag;
    dustPin = pin;
    dustMux = portMUX_INITIALIZER_UNLOCKED;
    reset_dust_sensor();
  }

  /**
     @brief Initialize variables associated with missed edge detection
   */
  void reset_suspect_diagnostics() {
    //  Serial.println("INFO: reset_suspect_diagnostics()");
    suspectISR    = 0;
    suspectState  = DustSensorState::STOPPED;
    suspectMillis = 0;
  }

  /**
     @brief Initialize variables associated with dust sensing and accounting
   */
  void reset_dust_sensor() {
    //  Serial.println("INFO: reset_dust_sensor()");
    prevState = currState;
    currState = DustSensorState::STOPPED;

    nTransitions           = 0;
    suspectSample          = false;
    detectionStartTime     = 0;
    totalDetectionDuration = 0;
    nDuplicateDust         = 0;
    nDuplicateClear        = 0;

    dustRatio     = 0.0;
    concentration = 0.0;

    reset_suspect_diagnostics();
  }

  /**
     @brief Set pin mode and attach ISR to pin

     @param isr_callback Pointer to a void function which takes no arguments
   */
  void initialize_pin(funcPtr isr_callback) {
    pinMode(dustPin, INPUT);

    //      attachInterrupt(digitalPinToInterrupt(dustPin), onPM1Change,
    // CHANGE);
    attachInterrupt(digitalPinToInterrupt(dustPin), isr_callback, CHANGE);
  }

  /**
     @brief Print sensor summary info
   */
  void print_sensor_info() {
    std::stringstream logss;

    logss << "INFO: Sensor " << dustTag << std::endl;
    logss << "INFO:   Total detection duration (us):  " <<
      totalDetectionDuration << std::endl;
    logss << "INFO:   Last detection start time (us): " << detectionStartTime <<
      std::endl;
    logss << "INFO:   Transitions:                    " << nTransitions <<
      std::endl;

    Serial.print(logss.str().c_str());
  }

  /**
     @brief Tag set accessor

     @param newtag Pin number of dust sensor line. Read-only.
   */
  void set_tag(const std::string& tag) {
    dustTag = tag;
  }

  /**
     @brief Tag set accessor

     @param newpin Text ID of dust sensor, e.g. "PM1", "PM2.5". Read-only.
   */
  void set_pin(const uint8_t pin) {
    dustPin = pin;
  }

  /**
     @brief Create aerosol log entry and append it to a logstream

     @param samplingDuration uint64_t sampling duration in microseconds
     @param logstream std::stringstream object for accumulating log entries
   */
  void add_to_logstream(const uint64_t   & samplingDuration,
                        std::stringstream& logstream) {
    auto t  = std::time(nullptr);
    auto tm = *std::gmtime(&t);

    std::stringstream ssnew;

    ssnew << "L=\"" << dustTag << "\"";
    ssnew << ", ID=\"" << acfg.device_id << "\"";
    ssnew << ", UTC=\"" << std::put_time(&tm, "%Y%m%dT%H%M%SZ") << "\"";

    if (suspectSample) {
      ssnew << ", ST=\"SUSPECT\", NDC=" << nDuplicateClear << ", NDD=" <<
        nDuplicateDust;
    } else {
      ssnew << ", ST=\"OK\", NDC=0, NDD=0";
    }
    ssnew << ", TS=" << samplingDuration;
    ssnew << ", TD=" << totalDetectionDuration;
    ssnew << ", R=" << std::setprecision(3) << dustRatio  << std::defaultfloat;
    ssnew << ", C=" << std::scientific << std::uppercase << std::setprecision(3)
          << concentration;
    checksum_logstream(ssnew);
    ssnew << std::endl;

    logstream << ssnew.str().c_str();
  }
};

#ifdef HAS_PM1

/// PM1 DustLPO object
DustLPO PM1;
#endif // ifdef HAS_PM1

#ifdef HAS_PM2_5

/// PM2.5 DustLPO object
DustLPO PM2_5;
#endif // ifdef HAS_PM2_5

#ifdef HAS_PM1
# ifdef HAS_PM2_5
const uint8_t nPM = 2;
DustLPO *PM[] { &PM1, &PM2_5 };
# else // ifdef HAS_PM2_5
const uint8_t nPM = 1;
DustLPO *PM[] { &PM1 };
# endif // ifdef HAS_PM2_5
#else // ifdef HAS_PM1
# ifdef HAS_PM2_5
const uint8_t nPM = 1;
DustLPO *PM[] { &PM2_5 };
# else // ifdef HAS_PM2_5
const uint8_t nPM = 0;
DustLPO *PM[] {};
# endif // ifdef HAS_PM2_5
#endif // ifdef HAS_PM1

// ======================================================================
// Filesystem info
// ======================================================================

#ifdef HAS_SPIFFS

/// Filesystem status
bool SPIFFS_filesystem_ok { false };

/// Seconds since last check of the SPIFFS filesystem
uint64_t last_SPIFFS_check { 0 };
#endif // ifdef HAS_SPIFFS

#ifdef HAS_SDCARD

/// SD filesystem status
bool SD_filesystem_ok { false };

/// Seconds since last check of the SPIFFS filesystem
uint64_t last_SD_check { 0 };
#endif // ifdef HAS_SDCARD

// ======================================================================
// MQ-2 gas sensor info
// ======================================================================

#ifdef HAS_MQ2

/**
   @brief MQ-2 gas sensor class
 */
class GasData {
public:

  /// MQ-2 gas sensor analog pin A0 connects to ADC pin 6 which connects
  //  to Node32s pin P34
  uint8_t analogPin { A6 };

  /// MQ-2 gas sensor digital pin D0 connects to ADC pin 7 which connects
  //  to Node32s pin P35
  uint8_t digitalPin { A7 };

  /// Digital pin data
  uint16_t D { 0 };

  /// Analog pin data
  uint16_t A { 0 };

  /**
     @brief Constructor
   */
  GasData(uint8_t APin = A6, uint8_t DPin = A7) {
    analogPin  = APin;
    digitalPin = DPin;

    initialize_pins();
    clear_data();
  }

  /**
     @brief Configure MQ-2 gas sensor pins
   */
  void initialize_pins() {
    pinMode(analogPin,  INPUT);
    pinMode(digitalPin, INPUT);
  }

  /**
     @brief Clear gas data
   */
  void clear_data() {
    A = 0;
    D = 0;
  }

  /**
     @brief Read gas data from sensor
   */
  void read_sensor() {
    A = analogRead(analogPin);

    if (digitalRead(digitalPin) == HIGH) {
      D = 1;
    } else {
      D = 0;
    }
  }

  /**
     @brief Read gas data from sensor
     @returns bool if sensor threshold has been exceeded (i.e. true if digital
     pin is HIGH; false otherwise)
   */
  bool above_threshold() {
    return D == 1;
  }

  /**
     @brief Create pressure/temperature sensor log entry and append it to a
     logstream

     @param gasInfo GasData object. Read-only
     @param logstream std::stringstream object for accumulating log entries
   */
  void add_to_logstream(std::stringstream& logstream) {
    auto t  = std::time(nullptr);
    auto tm = *std::gmtime(&t);

    std::stringstream ssnew;

    ssnew << "L=\"MQ-2\"";
    ssnew << ", ID=\"" << acfg.device_id << "\"";
    ssnew << ", UTC=\"" << std::put_time(&tm, "%Y%m%dT%H%M%SZ") << "\"";
    ssnew << ", GD=" << D;
    ssnew << ", GA=" << A;
    checksum_logstream(ssnew);
    ssnew << std::endl;

    logstream << ssnew.str().c_str();
  }
};

/// MQ-2 sensor reader object
GasData gasInfo;
#endif // ifdef HAS_MQ2

// ======================================================================
// Device and subsystem state machine definitions
// TODO: Refactor into states for dust sensor lines, sampling (ACTIVE/IDLE),
// data ready
// ======================================================================

/// Overall machine states, simplified by refactoring detection and data
//  acquisition states into their own state sets.
enum class DeviceState {
  COLD_START,
  WARMING,
  NO_DATA,
  WITH_DATA,
  WRITING_LOG,
  STOPPED
};

/**
   @brief Dust monitor state class
 */
class DeviceStatus {
public:

  /// Current device state
  DeviceState currState { DeviceState::COLD_START };

  /// Previous device state. Used for detecting and logging transitions
  DeviceState prevState { DeviceState::COLD_START };

  /**
     @brief Constructor
   */
  DeviceStatus() {
    currState = DeviceState::COLD_START;
    prevState = DeviceState::COLD_START;
  }

  /**
     @brief Save current state as previous state
   */
  void update_prev_state() {
    prevState = currState;
  }

  /**
     @brief Change current state and save history

     @param newState State to change to
   */
  void change_state_to(const DeviceState newState) {
    update_prev_state();
    currState = newState;
  }
};

/// Device state tracker
DeviceStatus overallState;

// ======================================================================
// Logging facilities
// ======================================================================

/// Logging output stream. Accumulates info throughout code, writes then
//  clears in log writing routine.
std::stringstream logstream;

/// File name of the log file last written to
std::string prevLogFile { "" };

/// File name of the log file currently being written to
std::string currLogFile { "/19700101_default_aerosmiff.log" };

/// Flag indicating the first record (presumed to have a bad sampling
//  interval) has not yet been detected.
//
//  TODO: Isolate the cause of the underlying error being masked by this flag
bool badFirstRecord { true };

// ======================================================================
// Status light info
// ======================================================================

// Note: The Node32S onboard LED pin number is internally defined as LED_BUILTIN

/// Number of on/off cycles which define a quick blink
const uint8_t LEDCycleLimit {
  3
};

// Status LED state
bool led_on { false };

// ======================================================================
// Interrupt service routine (ISR) definitions
// ======================================================================

// ISR1 was a callback to handle dust detection (transition to low on D0)
// ISR2 was a callback to handle dust non-detection (transition to high on D0)
// Both were merged into ISR4

/**
   @brief Generic dust pin change monitor interrupt service routine (ISR)

   Tracks low pulse occupancy (LPO) of a given dust sensor pin and
   updates the associated DustLPO object. This routine is riggers on
   CHANGE so the dust sensor pin must be polled to determine whether
   the change was RISING or FALLING. PM.currState holds the last state
   known for the pin - DETECTING if LOW, NOT_DETECTING if HIGH, and
   NO_DATA if unknown.

   Inconsistent state transitions (DETECTING to LOW, NOT_DETECTING to
   HIGH) result in a setting the PM.suspectState flag to true.
   NOT_DETECTING/NO_DATA to LOW transitions result in the DETECTING state
   being set and PM.detectionStartTime value being recorded.
   DETECTING/NO_DATA to HIGH transitions result in the NOT_DETECTING
   state being set, the PM.totalDetectionDuration value and the
   PM.detectionStartTime value being cleared. Low pulse duration and
   start time for NO_DATA cases are taken from the sampling interval
   start time.

   Inconsistent state transitions reset PM.detectionStartTime
   and LPO object state to the state consistent with the dust pin state
   and transition direction (RISING or FALLING). PM.nDuplicateDust and
   PM.nDuplicateClear are incremented and during the log-writing phase
   log entries will note the number of inconsistent transitions detected
   and mark the data record as SUSPECT to allow filtering potentially
   inaccurate readings.

   PM.suspectISR refers to an inconsistency detected in a FALLING
   change when the value is 1, and in a RISING change when the value
   is 2. These codes refer to a previous version of the code when
   FALLING transistions were handled by a routine refered to as ISR1 and
   RISING transitions were handled by ISR2.

   This routine is not directly registered as an ISR but is instead
   wrapped in a simple function or lambda which takes no arguments and
   associates a particular DustPLO instance with a particular dust
   sensor pin. This allows multiple dust sensor pins to be monitored
   for LPO with a minimum of redundant coding.

   @note This routine is tagged as IRAM_ATTR which is needed to
   to place the routine and its variables in a specific area of ESP32
   memory. PM.dustMux is used to mask interrupts on the same channel
   while this ISR is executing.

   @param PM DustLPO object associated with the dust sensor line
   @param dustPin Pin associated with the dust sensor line
   @param dustMux Mu(te?)x with the dust sensor line
 */
void IRAM_ATTR onDustPinChange(DustLPO     & PM,
                               const uint8_t dustPin,
                               portMUX_TYPE  dustMux) {
  // TODO: Verify if detection bug returns when &dustMux is replaced by
  // &PM.dustMux
  // void IRAM_ATTR onDustPinChange(DustLPO& PM) {
  if (sampleInterval.currState == DustSamplingState::ACTIVE) {
    //    portENTER_CRITICAL_ISR(&PM.dustMux);
    portENTER_CRITICAL_ISR(&dustMux);

    //    if (digitalRead(PM.dustPin) == LOW) {
    if (digitalRead(dustPin) == LOW) {
      switch (PM.currState) {
      case (DustSensorState::DETECTING): {
        // Shouldn't happen but does
        PM.suspectISR = 1;
        PM.nDuplicateDust++;
        PM.suspectState  = PM.currState;
        PM.suspectMillis = millis() - deviceStartTime;

        PM.suspectSample      = true;
        PM.detectionStartTime = micros();
        PM.nTransitions++;
        break;
      }

      case (DustSensorState::NOT_DETECTING): {
        PM.prevState          = PM.currState;
        PM.currState          = DustSensorState::DETECTING;
        PM.detectionStartTime = micros();
        PM.nTransitions++;
        break;
      }

      case (DustSensorState::NO_DATA): {
        PM.prevState          = PM.currState;
        PM.currState          = DustSensorState::DETECTING;
        PM.detectionStartTime = micros();
        PM.nTransitions++;
        break;
      }

      default: {
        // STOPPED state: Don't care
        break;
      }
      }
    } else {
      switch (PM.currState) {
      case (DustSensorState::NOT_DETECTING): {
        // Shouldn't happen but does
        PM.suspectISR = 2;
        PM.nDuplicateClear++;
        PM.suspectState  = PM.currState;
        PM.suspectMillis = millis() - deviceStartTime;

        PM.suspectSample = true;
        PM.nTransitions++;
        break;
      }

      case (DustSensorState::DETECTING): {
        PM.prevState               = PM.currState;
        PM.currState               = DustSensorState::NOT_DETECTING;
        PM.totalDetectionDuration += (micros() - PM.detectionStartTime);
        PM.detectionStartTime      = 0;
        PM.nTransitions++;
        break;
      }

      case (DustSensorState::NO_DATA): {
        PM.prevState              = PM.currState;
        PM.currState              = DustSensorState::NOT_DETECTING;
        PM.totalDetectionDuration = (micros() - sampleInterval.startMicros);
        PM.detectionStartTime     = 0;
        PM.nTransitions++;
        break;
      }

      default: {
        // STOPPED states: Don't care
        break;
      }
      }
    }

    //    portEXIT_CRITICAL_ISR(&PM.dustMux);
    portEXIT_CRITICAL_ISR(&dustMux);
  }
}

#ifdef HAS_PM1

/**
    @brief Dust pin D0 (PM1) ISR
 */
void IRAM_ATTR onPM1Change() {
  //  onDustPinChange(PM1);
  onDustPinChange(PM1, PM1Pin, PM1Mux);
}

#endif // ifdef HAS_PM1

#ifdef HAS_PM2_5

/**
    @brief Dust pin D1 (PM2.5) ISR
 */
void IRAM_ATTR onPM2_5Change() {
  //  onDustPinChange(PM2_5);
  onDustPinChange(PM2_5, PM2_5Pin, PM2_5Mux);
}

#endif // ifdef HAS_PM2_5

// ======================================================================
// Functions and subroutines
// ======================================================================

/**
   @brief Initialize digital pin LED_BUILTIN as an output.
 */
void setup_builtin_led() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  led_on = false;
}

/**
   @brief Set UTC time and date from serial (UNIX epoch) time

   @param serial_time time_t structure containing seconds since UNIX epoch.
 */
void setup_clock(const time_t serial_time) {
  // Compose struct with serial time and microsecond adjustment
  struct timeval tv;

  tv.tv_sec  = serial_time;
  tv.tv_usec = 0;

  // Set clock
  settimeofday(&tv, NULL);
}

/**
   @brief Return an ISO 8601 formatted timestamp (C string) based on serial
   time input. If input serial time is zero, use time from system clock.

   @param serial_time time_t structure containing seconds since UNIX epoch.

   @returns std::string formatted as <YYYY><MM><DD>T<HH><MM><SS>Z in UTC time
   compliant with ISO 8601
 */
std::string utc_timestamp(const time_t serial_time) {
  time_t my_serial_time;

  if (serial_time == 0) {
    my_serial_time = std::time(nullptr);
  } else {
    my_serial_time = serial_time;
  }

  auto tmlast = *std::gmtime(&my_serial_time);

  std::stringstream sststamp;
  sststamp << std::put_time(&tmlast, "%Y%m%dT%H%M%SZ");

  return std::string(sststamp.str().c_str());
}

/**
   @brief Print the current UTC timestamp to the serial console
 */
void print_utc_timestamp() {
  Serial.print("INFO: Clock set to ");

  auto t  = std::time(nullptr);
  auto tm = *std::gmtime(&t);

  std::stringstream tstamp;
  tstamp << std::put_time(&tm, "%Y%m%dT%H%M%SZ");
  Serial.println(tstamp.str().c_str());
}

/**
   @brief Return string description of DeviceState

   @param state DeviceState class enum

   @returns std::string object containing description of device state
 */
std::string DeviceStateAsText(DeviceState state) {
  std::string txt;

  switch (state) {
  case (DeviceState::COLD_START): {
    txt = "COLD_START";
    break;
  }

  case (DeviceState::WARMING): {
    txt = "WARMING";
    break;
  }

  case (DeviceState::NO_DATA): {
    txt = "NO_DATA";
    break;
  }

  case (DeviceState::WITH_DATA): {
    txt = "WITH_DATA";
    break;
  }

  case (DeviceState::WRITING_LOG): {
    txt = "WRITING_LOG";
    break;
  }

  case (DeviceState::STOPPED): {
    txt = "STOPPED";
    break;
  }

  default: {
    txt = "***UNKNOWN***";
    break;
  }
  }
  return txt;
}

/**
   @brief Return string description of DustSensorState

   @param state DeviceState class enum

   @returns std::string object containing description of dust sensor (DustLPO)
   state
 */
std::string SensorStateAsText(DustSensorState state) {
  std::string txt;

  switch (state) {
  case (DustSensorState::NO_DATA): {
    txt = "NO_DATA";
    break;
  }

  case (DustSensorState::DETECTING): {
    txt = "DETECTING";
    break;
  }

  case (DustSensorState::NOT_DETECTING): {
    txt = "NOT_DETECTING";
    break;
  }

  case (DustSensorState::STOPPED): {
    txt = "STOPPED";
    break;
  }

  default: {
    txt = "***UNKNOWN***";
    break;
  }
  }
  return txt;
}

/**
   @brief Return string description of DustSamplingState

   @param state DeviceState class enum

   @returns std::string object containing description of dust sampling interval
   state
 */
std::string SamplingStateAsText(DustSamplingState state) {
  std::string txt;

  switch (state) {
  case (DustSamplingState::ACTIVE): {
    txt = "ACTIVE";
    break;
  }

  case (DustSamplingState::INACTIVE): {
    txt = "INACTIVE";
    break;
  }

  default: {
    txt = "***UNKNOWN***";
    break;
  }
  }
  return txt;
}

/**
   @brief Initialize variables associated with dust sampling and accounting
 */
void reset_dust_accounting() {
  // TODO: Should critical section delimiters surround all variable assignments?

  sampleInterval.reset_sampling_interval();

  for (uint8_t i = 0; i < nPM; ++i) {
    portENTER_CRITICAL(&PM[i]->dustMux);
    PM[i]->reset_dust_sensor();
    portEXIT_CRITICAL(&PM[i]->dustMux);
  }
}

/**
   @brief Begin sampling interval. Reset dust accounting, enable timer and
   alarm,
   record interval start time, and activate sensors
 */
void begin_sampling_interval() {
  //  Serial.println("INFO: begin_sampling_interval()");

  reset_dust_accounting();

  sampleInterval.change_state_to(DustSamplingState::ACTIVE);

  for (uint8_t i = 0; i < nPM; ++i) {
    portENTER_CRITICAL(&PM[i]->dustMux);
    PM[i]->prevState = PM[i]->currState;
    PM[i]->currState = DustSensorState::NO_DATA;
    portEXIT_CRITICAL(&PM[i]->dustMux);
  }

  sampleInterval.endMillis = 0;
  sampleInterval.endMicros = 0;

  sampleInterval.startMicros = micros();
  sampleInterval.startMillis = millis();

  sampleInterval.nominalEndMillis = sampleInterval.startMillis +
                                    acfg.get_nominal_sample_millis();

  std::stringstream ssmsg;

  if (sampleInterval.startMillis > sampleInterval.nominalEndMillis) {
    ssmsg << "WARNING: startMillis > nominalEndMillis: " <<
      sampleInterval.startMillis
          << " > " << sampleInterval.nominalEndMillis << std::endl;
    Serial.print(ssmsg.str().c_str());
  }

  int16_t delta = sampleInterval.nominalEndMillis - sampleInterval.startMillis;

  if (delta < 0) {
    ssmsg.str(std::string());
    ssmsg << "Sample duration is negative; delaying by " << -delta << " ms" <<
      std::endl;
    Serial.print(ssmsg.str().c_str());

    delay(-delta);
  }
}

#ifdef HAS_BME280

/**
   @brief Setup BME280 pressure/temperature/humidity sensor
 */
void setup_PT_sensor() {
  Serial.println(F("INFO: Initializing pressure/temperature sensor."));

  // #define BME280_ADDRESS 0x76
  bool status = bme280.begin(BME280_ADDRESS);

  // TODO: Refactor this into something sensible
  if (!status) {
    Serial.println(F(
                     "PANIC: Cannot connect to BME280 pressure/temperature sensor. Halting."));

    while (1);
  }
}

#endif // ifdef HAS_BME280

#ifdef HAS_SPIFFS

/**
   @brief Set up SPIFFS filesystem
 */
void setup_SPIFFS_filesystem() {
  std::stringstream ssmsg;

  SPIFFS_filesystem_ok = SPIFFS.begin(true);

  if (SPIFFS_filesystem_ok) {
    ssmsg << "INFO: SPIFFS filesystem formatted and operable." << std::endl;
  } else {
    uint8_t ict = 10;
    int     iostat;

    while (!SPIFFS_filesystem_ok && ict > 0) {
      delay(1000);
      iostat               = SPIFFS.begin(true);
      SPIFFS_filesystem_ok = (iostat == 0);

      if (SPIFFS_filesystem_ok) {
        ssmsg << "INFO: SPIFFS filesystem formatted and operable." << std::endl;
      } else {
        ssmsg << "ERROR: SPIFFS filesystem inoperable, error code " << iostat <<
          ", "
              << ict << " tries remaining." << std::endl;
      }
    }
  }
  Serial.print(ssmsg.str().c_str());
}

#endif // ifdef HAS_SPIFFS

/**
   @brief Add NMEA-style checksum to a logstream

   @param ss std::stringstream object containing a single log line with no
   terminating EOL character
 */
void checksum_logstream(std::stringstream& ss) {
  int checksum { 0 };

  for (auto it = ss.str().cbegin(); it != ss.str().cend(); ++it) {
    checksum ^= *it;
  }
  ss << "*" << std::uppercase << std::setfill('0') << std::setw(2)
     << std::hex << checksum << std::dec;
}

#ifdef HAS_SPIFFS

/**
   @brief Test the SPIFFS filesystem
 */
void test_SPIFFS_filesystem() {
  const std::string testfn("/test.txt");
  const std::string badtestfn("/nonexistance_check.txt");
  int iostat;

  if (!SPIFFS_filesystem_ok) {
    Serial.println(F("ERROR: SPIFFS not mounted."));
    return;
  }

  if (SPIFFS.exists(testfn.c_str())) {
    // Stray test file - delete it
  }

  if (SPIFFS.exists(badtestfn.c_str())) {
    // Nonexistant test file check; FS problem if this exists
    Serial.println(F("ERROR: Found nonexistant file"));
    SPIFFS_filesystem_ok = false;
    return;
  }

  File file = SPIFFS.open(testfn.c_str(), FILE_WRITE);

  if (!file) {
    Serial.println(("ERROR: Cannot open test file for writing."));
    SPIFFS_filesystem_ok = false;
    return;
  }

  iostat = file.print("TEST");

  if (!iostat) {
    Serial.println(F("ERROR: Cannot write to open test file."));
    Serial.print(F("ERROR: iostat = "));
    Serial.println(iostat);
    SPIFFS_filesystem_ok = false;
  }

  file.close();

  file = SPIFFS.open(testfn.c_str(), FILE_READ);

  if (!file) {
    Serial.println(F("FS Error: Cannot open test file for reading."));
    SPIFFS_filesystem_ok = false;
    return;
  } else {
    // TODO: Check test file contents
    file.close();
  }

  SPIFFS.remove(testfn.c_str());
}

#endif // ifdef HAS_SPIFFS

/**
   @brief Print device characteristics such as firmware version, device ID,
   and other information to the serial console
 */
void print_device_characteristics() {
  std::stringstream ssmsg;

  ssmsg <<
    "INFO: =========================================================================="
        << std::endl;
  ssmsg << "INFO: Aerosniff: ESP32-Based Air Quality Sensor" << std::endl;
  ssmsg <<
    "INFO: =========================================================================="
        << std::endl;
  ssmsg << "INFO: Firmware Version: " << VERSION_ID << std::endl;
  ssmsg << "INFO: Contact info:     " << CONTACT_INFO << std::endl;
  ssmsg <<
    "INFO: --------------------------------------------------------------------------"
        << std::endl;
  ssmsg << "INFO: Device ID:                     " << acfg.device_id.c_str() << std::endl;
  ssmsg << "INFO: Provisioned serial time (UTC): " << acfg.utctime_frozen << std::endl;
  ssmsg << "INFO: Sensor warmup delay (s):       " << acfg.sensor_warmup_time << std::endl;
  ssmsg << "INFO: Nominal sampling interval (s): " << acfg.nominal_sample_time <<
    std::endl;
  ssmsg << "INFO: Current device time:           " <<
    utc_timestamp(std::time(nullptr)).c_str() << std::endl;
  ssmsg <<
    "INFO: --------------------------------------------------------------------------"
        << std::endl;
  ssmsg << "INFO: Log entry fields:" << std::endl;
  ssmsg << "INFO:   All entries:" << std::endl;
  ssmsg << "INFO:     L    = log entry type, string (GPS|PM1|PM2.5|PT|MQ-2)" <<
    std::endl;
  ssmsg << "INFO:     ID   = device ID, string (\"" << acfg.device_id.c_str() <<
    "\")" << std::endl;
  ssmsg << "INFO:     UTC  = ISO 8601 date/timestamp in UTC" << std::endl;
#ifdef HAS_GPS
  ssmsg << "INFO:   L=\"GPS\" entries:" << std::endl;
  ssmsg << "INFO:     LAT  = latitude" << std::endl;
  ssmsg << "INFO:     LNG  = longitude" << std::endl;
  ssmsg << "INFO:     ALT  = altitude above mean sea level, m" << std::endl;
#endif // ifdef HAS_GPS

  if (nPM > 0) {
    ssmsg << "INFO:   L=\"PM1\" and \"PM2.5\" entries:" << std::endl;
    ssmsg << "INFO:     ST   = aerosol data status (OK|SUSPECT)" << std::endl;
    ssmsg <<
      "INFO:     NDC  = number of duplicate aerosol sensor clear interrupts found during sampling period"
          << std::endl;
    ssmsg <<
      "INFO:     NDD  = number of duplicate aerosol detected interrupts found during sampling period"
          << std::endl;
    ssmsg << "INFO:     TS   = duration of sampling period, in microseconds" <<
      std::endl;
    ssmsg <<
      "INFO:     TD   = total time aerosol was detected during sampling period, in microseconds"
          << std::endl;
    ssmsg <<
      "INFO:     R    = percent of sampling period when aerosol was dectected (100 * TD / TS)"
          << std::endl;
    ssmsg << "INFO:     C    = aerosol concentration, in particles / liter" <<
      std::endl;
  }
#ifdef HAS_BME280
  ssmsg << "INFO:   L=\"PT\" entries:" << std::endl;
  ssmsg << "INFO:     PATM = ambient air pressure, Pa" << std::endl;
  ssmsg << "INFO:     TATM = ambient air temperature, C" << std::endl;
  ssmsg << "INFO:     RH   = relative humidity, %" << std::endl;
#endif // ifdef HAS_BME280
#ifdef HAS_MQ2
  ssmsg << "INFO:   L=\"MQ-2\" entries:" << std::endl;
  ssmsg << "INFO:     GD   = MQ-2 gas sensor digital output" << std::endl;
  ssmsg << "INFO:     GA   = MQ-2 gas sensor analog output" << std::endl;
#endif // ifdef HAS_MQ2
  ssmsg <<
    "INFO: Every log entry begins with the L, ID, and UTC fields, contains various fields determined by"
        << std::endl;
  ssmsg <<
    "INFO: the L field value, and is terminated with '*HH' which is the cumulative XOR checksum of the"
        << std::endl;
  ssmsg <<
    "INFO: log entry in hexadecimal. See NMEA checksum for details on calculating and validating"
        << std::endl;
  ssmsg << "INFO: the log entry checksum." << std::endl;
#ifdef HAS_GPS
  ssmsg <<
    "INFO: L=\"GPS\" entries contain L, ID, UTC, LAT, LNG, and ALT fields." <<
    std::endl;
#endif // ifdef HAS_GPS

  if (nPM > 0) {
    ssmsg <<
      "INFO: L=\"PM1\" and \"PM2.5\" entries contain L, ID, UTC, TS, ST, NDC, NDD, TD, R, and C fields."
          << std::endl;
  }
#ifdef HAS_BME280
  ssmsg <<
    "INFO: L=\"PT\" entries contain L, ID, UTC, PATM, TATM, and RH fields." <<
    std::endl;
#endif // ifdef HAS_BME280
#ifdef HAS_MQ2
  ssmsg << "INFO: L=\"MQ-2\" entries contain L, ID, UTC, GD, and GA fields." <<
    std::endl;
#endif // ifdef HAS_MQ2

  //  Serial.println("PANIC: 'Q' % '\\f' == '\\t'");
  ssmsg <<
    "INFO: =========================================================================="
        << std::endl;

  Serial.print(ssmsg.str().c_str());
}

/**
   @brief Print a list of all files in SPIFFS

   @param fs fs::FS object
   @param dirname C-string containing initial directory name, typically "/"
   @param levels uint8_t number of levels to recurse into
   @param burn Optional flag to delete all files found. Default is false (do not
   delete)
 */
void list_dir(fs::FS& fs, const std::string& dirname = "/", uint8_t levels = 0,
              bool burn                              = false) {
  std::stringstream ssmsg;

  ssmsg << "INFO: Listing directory: " << dirname;

  File root = fs.open(dirname.c_str());

  if (!root) {
    ssmsg << " - failed to open directory" << std::endl;
    Serial.print(ssmsg.str().c_str());
    return;
  } else if (!root.isDirectory()) {
    ssmsg << " - not a directory" << std::endl;
    Serial.print(ssmsg.str().c_str());
    return;
  } else {
    ssmsg << std::endl;
    Serial.print(ssmsg.str().c_str());
  }

  File file = root.openNextFile();

  while (file) {
    ssmsg.str(std::string());

    if (file.isDirectory()) {
      ssmsg << "INFO:  DIR : " << file.name() << std::endl;

      if (levels) {
        list_dir(fs, file.name(), levels - 1, burn);
      }
    } else {
      ssmsg << "INFO:  FILE: " << file.name() << "  SIZE: " << file.size() <<
        std::endl;

      // TESTING - deletes all files
      if (burn) {
        ssmsg << "NOTICE: Removing " << file.name() << std::endl;
        fs.remove(file.name());
      }
    }
    Serial.print(ssmsg.str().c_str());
    file = root.openNextFile();
  }
}

#ifdef HAS_SDCARD

/**
   @brief Enable the SD filesystem over SPI and print cursory diagnostics
 */
void enable_SD_filesystem() {
  std::stringstream ssmsg;

  ssmsg << "INFO: Setting up SD card filesystem." << std::endl;

  if (!SD.begin()) {
    ssmsg << "INFO: Cannot mount SD card interface." << std::endl;
  } else {
    ssmsg << "INFO: Mounted SD card interface." << std::endl;

    uint8_t cardType = SD.cardType();

    switch (cardType) {
    case (CARD_NONE): {
      ssmsg << "WARNING: No SD card attached." << std::endl;
      break;
    }

    case (CARD_MMC): {
      SD_filesystem_ok = true;
      ssmsg << "INFO: SD Card Type: MMC" << std::endl;
      break;
    }

    case (CARD_SD): {
      SD_filesystem_ok = true;
      ssmsg << "INFO: SD Card Type: SDSC" << std::endl;
      break;
    }

    case (CARD_SDHC): {
      SD_filesystem_ok = true;
      ssmsg << "INFO: SD Card Type: SDHC" << std::endl;
      break;
    }

    default: {
      ssmsg << "INFO: SD Card Type: UNKNOWN" << std::endl;
      break;
    }
    }

    if (cardType != CARD_NONE) {
      uint64_t cardSize = SD.cardSize();
      ssmsg << "INFO: SD Card Size: " << (cardSize / (1024 * 1024))
            << " MB, " << (cardSize / 1000000) << " MiB" << std::endl;
    }
    Serial.print(ssmsg.str().c_str());
  }
}

#endif // ifdef HAS_SDCARD

/**
   @brief End the sampling interval and finalize accounting of low pulse
   intervals on the dust sensor lines
 */
void end_sampling_interval() {
  //  std::stringstream ssmsg;
  sampleInterval.endMicros = micros();
  sampleInterval.endMillis = millis();

  sampleInterval.change_state_to(DustSamplingState::INACTIVE);

  overallState.change_state_to(DeviceState::WITH_DATA);

  for (int i = 0; i < nPM; ++i) {
    switch (PM[i]->currState) {
    case (DustSensorState::DETECTING): {
      // Truncate detection pulse
      PM[i]->totalDetectionDuration += (micros() - PM[i]->detectionStartTime);

      PM[i]->prevState = PM[i]->currState;
      PM[i]->currState = DustSensorState::STOPPED;
      break;
    }

    case (DustSensorState::NOT_DETECTING): {
      PM[i]->prevState = PM[i]->currState;
      PM[i]->currState = DustSensorState::STOPPED;
      break;
    }

    case (DustSensorState::NO_DATA): {
      if (digitalRead(PM[i]->dustPin) == LOW) {
        PM[i]->totalDetectionDuration =
          (sampleInterval.endMicros - sampleInterval.startMicros);
      } else {
        PM[i]->totalDetectionDuration = 0;
      }

      PM[i]->prevState = PM[i]->currState;
      PM[i]->currState = DustSensorState::STOPPED;
      break;
    }

    default: {
      // Don't care
      break;
    }
    }
  }
}

/**
   @brief Calculate and format loggable data
 */
void record_loggable_data() {
  static std::stringstream ssmsg;

  double deviation1         { 0.0 };
  double deviation2         { 0.0 };

  // TODO: Refactor calculations
  uint64_t samplingDuration = sampleInterval.endMicros -
                              sampleInterval.startMicros;
  uint32_t samplingMillis = sampleInterval.endMillis -
                            sampleInterval.startMillis;

  deviation1 = 0.0;

  if (samplingMillis > 0) {
    deviation1 =
      abs(1.0 - ((double)(samplingDuration / 1000) / ((double)samplingMillis)));

    if (deviation1 > 0.01) {
      // Drop data frames which have a substantial mismatch between sampling
      // intervals
      // measured in milliseconds and microseconds
      overallState.change_state_to(DeviceState::NO_DATA);

      if (!badFirstRecord) {
        ssmsg <<
          "WARNING: Deviation between samplingDuration/1000 (ms) and samplingMillis (ms) (%): "
              << std::setprecision(5) << (100.0 * deviation1) << std::endl
              << "WARNING: Ignoring data frame." << std::endl;
        Serial.print(ssmsg.str().c_str());
      }
    }
  } else {
    if (!badFirstRecord) {
      ssmsg.str("WARNING: Measured sample interval is too small (ms): ");
      ssmsg << samplingMillis << std::endl;
      Serial.print(ssmsg.str().c_str());
    }

    for (uint8_t i = 0; i < nPM; ++i) {
      portENTER_CRITICAL(&PM[i]->dustMux);
      PM[i]->suspectSample = true;
      portEXIT_CRITICAL(&PM[i]->dustMux);
    }
  }

  deviation2 = abs(1.0 - ((double)samplingMillis / (double)acfg.get_nominal_sample_millis()));

  if (deviation1 > 0.01) {
    // Do nothing; state should have already been set to IDLE_NO_DATA
  } else if (deviation2 > 0.01) {
    // Drop data frames which have a sample window > +/-1% of nominal
    overallState.change_state_to(DeviceState::NO_DATA);

    if (!badFirstRecord) {
      ssmsg.str("WARNING: Deviation from nominal sampling interval (%): ");
      ssmsg << std::setprecision(5) << (100.0 * deviation2) << std::endl;
      Serial.print(ssmsg.str().c_str());

      if (samplingMillis < acfg.get_nominal_sample_millis()) {
        ssmsg.str("INFO: Truncated sampling interval found (ms): TS=");
      } else {
        ssmsg.str("INFO: Inflated sampling interval found (ms): TS=");
      }
      ssmsg << samplingMillis << std::endl;
      Serial.print(ssmsg.str().c_str());
    }
  } else {
    // Build log entries and append to logstream

    for (uint8_t i = 0; i < nPM; ++i) {
      // Append PM1 log entry to logstream
      // Scaled to hundredths of a percent
      // (100x fraction of time dust was detected)
      portENTER_CRITICAL(&PM[i]->dustMux);
      PM[i]->dustRatio = (100.0 * (double)PM[i]->totalDetectionDuration) /
                         ((double)samplingDuration);
      PM[i]->concentration =
        ((1.1 * PM[i]->dustRatio - 3.8) * PM[i]->dustRatio + 520.0) *
        PM[i]->dustRatio + 0.62;
      PM[i]->add_to_logstream(samplingDuration, logstream);
      portEXIT_CRITICAL(&PM[i]->dustMux);
    }

#ifdef HAS_BME280

    // Append pressure/temperature sensor log entry to logstream
    localAir.read_sensor();
    localAir.add_to_logstream(logstream);
#endif // ifdef HAS_BME280

#ifdef HAS_MQ2

    // Append MQ-2 combustible gas sensor log entry to logstream
    gasInfo.read_sensor();
    gasInfo.add_to_logstream(logstream);
#endif // ifdef HAS_MQ2

    overallState.change_state_to(DeviceState::WRITING_LOG);
  }
}

/**
   @brief Generate log file name from current time and device ID.

   @returns std::string containing date-stamped device-specific log name.
 */
std::string generate_log_name() {
  auto t  = std::time(nullptr);
  auto tm = *std::gmtime(&t);

  static std::stringstream ofnstream;

  ofnstream.str(std::string());
  ofnstream << "/" <<
    std::put_time(&tm, "%Y%m%d") << "_" << acfg.device_id << "_aerosniff.log";

  return std::string(ofnstream.str().c_str());
}

/**
   @brief Write log stream to file
 */
void write_log_entries() {
  static std::stringstream ssmsg;

  currLogFile = generate_log_name();

  // Set previous log file name to current file name if previous is blank
  if (prevLogFile.length() < 10) {
    prevLogFile = currLogFile;

    ssmsg << "INFO: Setting log file to \"" << currLogFile << "\"" << std::endl;
    ssmsg <<
      "INFO: =========================================================================="
          << std::endl;
    Serial.print(ssmsg.str().c_str());
  }

  // Note log rotation
  if (prevLogFile != currLogFile) {
    ssmsg.str(std::string());
    ssmsg << "INFO: Log file \"" << prevLogFile <<
      "\" has rotated; new file name is \""
          << currLogFile << "\"" << std::endl;
    ssmsg <<
      "INFO: =========================================================================="
          << std::endl;
    Serial.print(ssmsg.str().c_str());
  }

  // Write log entry to SD card log file
#ifdef HAS_SDCARD

  if (SD_filesystem_ok) {
    yield();
    File file = SD.open(currLogFile.c_str(), FILE_APPEND);

    if (!file) {
      ssmsg.str(std::string());
      ssmsg << "ERROR: Cannot write to SD log file \"" << currLogFile << "\"" <<
        std::endl;
      Serial.print(ssmsg.str().c_str());
    } else {
      if (!file.print(logstream.str().c_str())) {
        ssmsg.str(std::string());
        ssmsg << "ERROR: Cannot write new line to SD log file \"" <<
          currLogFile << "\"" << std::endl;
        Serial.print(ssmsg.str().c_str());
      }
      file.close();
    }
    yield();
  } else {
    ssmsg.str("ERROR: SD card not mounted.");
    ssmsg << std::endl;
    Serial.print(ssmsg.str().c_str());
  }
#endif // ifdef HAS_SDCARD

  Serial.print(logstream.str().c_str());

  // Reset log stringstream
  logstream.str(std::string());

  // Update history
  prevLogFile = currLogFile;
}

#ifdef HAS_SPIFFS
# ifdef HAS_SDCARD

/**
 * @brief Copy configuration file from SD card to ESP32 SPIFFS filesystem
 * @param filename std:string containing configuration file 
 *        name including leading path ("/")
 */
void copy_conffile_from_SD_card(const std::string filename) {
//  const std::string filename("/aerosniff.conf");
  std::stringstream ssmsg;
  // Blocksize for reading from SD card
  const size_t LBUF = 512;
  static uint8_t buf[LBUF];

  if (SD_filesystem_ok) {
    File infile = SD.open(filename.c_str(), FILE_READ);

    if (!infile) {
      ssmsg << "ERROR: Cannot read from configuration file \""
            << filename.c_str() << "\" on SD card" << std::endl;
      Serial.print(ssmsg.str().c_str());
      ssmsg.str(std::string());
    } else {
      if (SPIFFS_filesystem_ok) {
        File outfile = SPIFFS.open(filename.c_str(), FILE_WRITE);

        if (!outfile) {
          ssmsg << "ERROR: Cannot write to configuration file \""
                << filename.c_str() << "\" on ESP32" << std::endl;
          Serial.print(ssmsg.str().c_str());
          ssmsg.str(std::string());
        } else {
          size_t len = infile.size();
          while(infile.available()) {
            while (len){
              size_t toRead = len;
              if (toRead > LBUF){
                toRead = LBUF;
              }
              infile.read(buf, toRead);
              outfile.write(buf, toRead);
              len -= toRead;
            }
          }
          outfile.flush();
          outfile.close();
          ssmsg << "INFO: Copied \""
                << filename.c_str() << "\" from SD card to SPIFFS"
                << std::endl;
          Serial.print(ssmsg.str().c_str());
          ssmsg.str(std::string());
          // TODO: Verify file copy
        }
      } else {
        // Can't mount SPIFFS
          ssmsg << "ERROR: Cannot read from SPIFFS filesystem"
                << std::endl;
          Serial.print(ssmsg.str().c_str());
          ssmsg.str(std::string());
      }
      infile.close();
    }
  } else {
    // Can't mount SD card
    ssmsg << "ERROR: Cannot read from SD card filesystem"
          << std::endl;
    Serial.print(ssmsg.str().c_str());
    ssmsg.str(std::string());
  }
}

# endif // ifdef HAS_SDCARD
#endif  // ifdef HAS_SPIFFS

/**
   @brief Blink the onboard LED

   @param led_on. Intended state of status LED; read-write.
 */
void blink_quickly(bool& led_on) {
  for (int i = 0; i < LEDCycleLimit * 2; ++i) {
    if (led_on) {
      digitalWrite(LED_BUILTIN, LOW);
      led_on = false;
    } else {
      digitalWrite(LED_BUILTIN, HIGH);
      led_on = true;
    }
    delay(100);
  }
}

// ======================================================================
// Arduino setup and 'superloop' routines
// ======================================================================

/**
   @brief Initialize the dust monitor and all attached peripherals
 */
void setup() {
  std::stringstream ssmsg;

  deviceStartTime = millis();

  Serial.begin(CONSOLE_BAUD);

  overallState.change_state_to(DeviceState::COLD_START);
  overallState.prevState = overallState.currState;

  // Initialize built-in LED
  setup_builtin_led();

  // Short delay to allow peripherals time to boot and quiesce
  ssmsg << "NOTICE: Rebooted aerosniff"  << std::endl
        << "INFO: Beginning " << acfg.peripheral_boot_delay
        << " second quiescent delay" << std::endl;
  Serial.print(ssmsg.str().c_str());
  ssmsg.str(std::string());

  digitalWrite(LED_BUILTIN, HIGH);
  led_on = true;

  delay(acfg.get_peripheral_boot_millis());

  digitalWrite(LED_BUILTIN, LOW);
  led_on = false;

  ssmsg << "INFO: End quiescent delay" << std::endl;
  Serial.print(ssmsg.str().c_str());
  ssmsg.str(std::string());

  // SD card setup
#ifdef HAS_SDCARD
  enable_SD_filesystem();
#endif // ifdef HAS_SDCARD

#ifdef HAS_SPIFFS

  // Configure SPIFFS filesystem
  setup_SPIFFS_filesystem();
#endif // ifdef HAS_SPIFFS

#ifdef HAS_GPS

  // Configure GPS
  GPSPos.updateInterval = acfg.gps_check_interval;
  GPSPos.initialize_receiver(gpsTXmcuRX, gpsRXmcuTX);
#endif // ifdef HAS_GPS

  // Set system clock (from provisioning, GPS, NTP...)
  setup_clock(acfg.utctime_frozen);

  // Print device information to serial console
  print_device_characteristics();

#ifdef HAS_SPIFFS

  // Test filesystem
  test_SPIFFS_filesystem();

  ssmsg.str(std::string());

  if (SPIFFS_filesystem_ok) {
    ssmsg << "INFO: SPIFFS filesystem seems ok." << std::endl;
    ssmsg << "INFO: SPIFFS filesystem contains:" << std::endl;
    Serial.print(ssmsg.str().c_str());
    ssmsg.str(std::string());

    list_dir(SPIFFS, "/", 0, false);
    last_SPIFFS_check = millis();
  } else {
    ssmsg << "ERROR: Problem mounting SPIFFS filesystem." << std::endl;
    Serial.print(ssmsg.str().c_str());
    ssmsg.str(std::string());
  }
#endif // ifdef HAS_SPIFFS

#ifdef HAS_SDCARD

  if (SD_filesystem_ok) {
    ssmsg << "INFO: SD filesystem seems ok." << std::endl;
    ssmsg << "INFO: SD filesystem contains:" << std::endl;
    Serial.print(ssmsg.str().c_str());
    ssmsg.str(std::string());

    list_dir(SD, "/", 0);

    last_SD_check = millis();
  } else {
    ssmsg << "ERROR: Problem mounting SD filesystem." << std::endl;
    Serial.print(ssmsg.str().c_str());
    ssmsg.str(std::string());
  }

# ifdef HAS_SPIFFS

  const std::string conffile("/aerosniff.conf");
  File file = SD.open(conffile.c_str(), FILE_READ);
  if (file) {
    file.close();
    copy_conffile_from_SD_card(conffile);

    ssmsg << "INFO: Configuration file \""
          << conffile << "\"on SD card copied to SPIFFS"
          << std::endl;
  } else {
    ssmsg << "INFO: No configuration file found on SD card"
          << std::endl;
  }
  Serial.print(ssmsg.str().c_str());
  ssmsg.str(std::string());

# endif // ifdef HAS_SPIFFS

#endif // ifdef HAS_SDCARD

#ifdef HAS_PM1
  PM1.set_tag("PM1");
  PM1.set_pin(PM1Pin);
  PM1.reset_dust_sensor();
  PM1.initialize_pin(onPM1Change);
#endif // ifdef HAS_PM1

#ifdef HAS_PM2_5
  PM2_5.set_tag("PM2.5");
  PM2_5.set_pin(PM2_5Pin);
  PM2_5.reset_dust_sensor();
  PM2_5.initialize_pin(onPM2_5Change);
#endif // ifdef HAS_PM2_5

#ifdef HAS_MQ2

  // Setup MQ-2 gas sensor
  gasInfo.initialize_pins();
#endif // ifdef HAS_MQ2

#ifdef HAS_BME280

  // Initialize pressure/temperature sensor
  setup_PT_sensor();
#endif // ifdef HAS_BME280

  overallState.change_state_to(DeviceState::WARMING);

  // Reset log stringstream
  logstream.str(std::string());
}

/**
   @brief Implement device, interval, and sensor state transitions and perform
   actions required during each state transition
 */
void loop() {
  std::stringstream ssmsg;

  switch (overallState.currState) {
  case (DeviceState::WARMING): {
    // Hold in WARMING state until warming time complete

    // Change state to NO_DATA after sensor is warm
    if (millis() - deviceStartTime > acfg.get_sensor_warmup_millis()) {
      overallState.change_state_to(DeviceState::NO_DATA);

      // Turn off LED at end of warming period
      if (led_on) {
        digitalWrite(LED_BUILTIN, LOW);
        led_on = false;
      }
    } else {
      // LED is solid on to indicate warming period
      if (!led_on) {
        digitalWrite(LED_BUILTIN, HIGH);
        led_on = true;
      }
    }
    break;
  }

  case (DeviceState::NO_DATA): {
    switch (sampleInterval.currState) {
    case (DustSamplingState::ACTIVE): {
      // Do nothing while sensors are taking data

      // End sampling interval if nominal interval end time is exceeded
      if ((sampleInterval.nominalEndMillis > 0)
          && (millis() > sampleInterval.nominalEndMillis)) {
        end_sampling_interval();
      }

      break;
    }

    case (DustSamplingState::INACTIVE): {
      // Check for periodic maintenance tasks like updating time from GPS
#ifdef HAS_GPS
      yield();
      GPSPos.adjust_spacetime();

      if (GPSPos.updated()) {
        GPSPos.add_to_logstream(logstream);
      }
      yield();
#endif // ifdef HAS_GPS

#ifdef HAS_SDCARD

      if (millis() > (last_SD_check + acfg.get_filesystem_list_millis())) {
        ssmsg.str(std::string());
        ssmsg << "INFO: Listing contents of SD card" << std::endl;
        Serial.print(ssmsg.str().c_str());
        list_dir(SD, "/", 0);
        last_SD_check = millis();
      }
#endif // ifdef HAS_SDCARD

#ifdef HAS_SPIFFS

      if (millis() > (last_SPIFFS_check + acfg.get_filesystem_list_millis())) {
        ssmsg.str(std::string());
        ssmsg << "INFO: Listing contents of SPIFFS filesystem" << std::endl;
        Serial.print(ssmsg.str().c_str());
        list_dir(SPIFFS, "/", 0);
        last_SPIFFS_check = millis();
      }
#endif // ifdef HAS_SPIFFS

      // Enable timer alarm and activate sensors
      begin_sampling_interval();

      break;
    }

    default: {
      // Should never get here
      break;
    }
    }

    break;
  }

  case (DeviceState::WITH_DATA): {
    //      Serial.println("In DeviceState::WITH_DATA");
    blink_quickly(led_on);

    // Calculate and format loggable data
    record_loggable_data();

    badFirstRecord = false;

    break;
  }

  case (DeviceState::WRITING_LOG): {
    //      Serial.println("In DeviceState::WRITING_LOG");

    blink_quickly(led_on);

    write_log_entries();

    overallState.change_state_to(DeviceState::NO_DATA);

    break;
  }

  case (DeviceState::STOPPED): {
    ssmsg << "In DeviceState::STOPPED: " << std::endl;

    ssmsg << "STOPPED is not implemented; not sure how this happened..." <<
      std::endl;
    Serial.print(ssmsg.str().c_str());
    break;
  }

  default: {
    // Do nothing
    break;
  }
  }

  // Update history
  overallState.prevState = overallState.currState;

  sampleInterval.prevState = sampleInterval.currState;

  for (uint8_t i = 0; i < nPM; ++i) {
    portENTER_CRITICAL(&PM[i]->dustMux);
    PM[i]->prevState = PM[i]->currState;
    portEXIT_CRITICAL(&PM[i]->dustMux);
  }
}
