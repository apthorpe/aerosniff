# README #

Aerosniff is Arduino-ESP32 firmware for a low cost atmospheric sensor which records aerosol and combustible gas concentration, ambient air conditions, and GPS time and position information.

## Hardware ##

A breadboard diagram of the Aerosniff development hardware can be found at https://bitbucket.org/apthorpe/aerosniff/src/default/userdoc/aerosniff_bb.png along with the source Fritzing file https://bitbucket.org/apthorpe/aerosniff/src/default/userdoc/aerosniff.fzz Grove connectors are not explicitly shown; instead the Grove wiring color convention is retained for clarity.

The prototype device used for system development is based on a Node32s-compatible ESP32 WROOM32-based development board (https://www.amazon.com/gp/product/B0718T232Z/) and uses several Grove-compatible sensors available from Seeed Studios, including:

* Grove Dust Sensor http://wiki.seeedstudio.com/Grove-Dust_Sensor/
* Grove Barometric Sensor (BME280) http://wiki.seeedstudio.com/Grove-Barometer_Sensor-BME280/
* Grove GPS http://wiki.seeedstudio.com/Grove-GPS/

Additional non-Grove sensors include:

* MQ-2 Combustible Gas Sensor Breakout Board https://www.amazon.com/gp/product/B0778NQ5JK/
* SD Card Reader Breakout Board (SPI-based) https://www.amazon.com/gp/product/B01MSNX0TW/

All sensors are driven by 5V. The firmware source code documents the connections between the sensor data lines and the I/O pins on the ESP32 board.

Note that the original dust sensor cable was modified to add a connection to the D1 pin (PM2.5 aerosol sensor data). This allows two particle size channels to be monitored instead of the single PM1 sensor line present in the original cable. The remaining unconnected pin in the 5-pin JST XH connector is the voltage threshold adjustment pin. Additionally, the open face of the dust sensor was covered as suggested by the manufacturer to prevent ambient light from entering the detection chamber and skewing the results. This also has the benefit of creating a 'chimney' effect within the sensor to draw air past the detector.

For conversion of all peripherals to the Grove interface standard, the Grove connector format is the NS Tech 1125 format described at http://www.nstech.com.cn/en/products_1.asp?menuid=19&menuid2=38&id=14 using a 4-pin keyed connector on a 2mm pitch. These are similar to **but not compatible with** JST PH-series connectors; the primary difference is the 1125 connector has latching clips on the sides which do not fit into the PH sockets.

## Dependencies ##

The firmware relies on the following libraries:

* Adafruit BME280 barometric sensor library ported to the ESP32 (https://github.com/Takatsuki0204/BME280-I2C-ESP32)
* TinyGPS++ library (https://github.com/mikalhart/TinyGPSPlus)
* FS, SD, and SPIFFS filesystem libraries for Arduino-ESP32 (https://github.com/espressif/arduino-esp32)
    
The remaining libraries used are all C/C++ standard libraries. Only the BME280 and TinyGPS++ libraries need to be installed from external sources; the remainder are supplied by the Arduino-ESP32 distribution maintained by Espressif Systems.

Use `doxygen Doxyfile` to generate developer documentation for the firmware; issuing a `make` command in `./doc/latex` will generate PDF documentation in the file `refman.pdf`.

## Sample Output ##

Example serial console output resembles:

    INFO: ==========================================================================
    INFO: Aerosniff: ESP32-Based Air Quality Sensor
    INFO: ==========================================================================
    INFO: Firmware Version: 20181102_1220
    INFO: Contact info: Bob Apthorpe <bob.apthorpe@acorvid.com>
    INFO: Device ID: P001
    INFO: Provisioned serial time (UTC): 1541179248
    INFO: Sensor warmup delay (ms): 30000
    INFO: Nominal sampling interval (ms): 30000
    INFO: Current device time: 20181102T172048Z
    INFO: Log entry fields:
    INFO:   All entries:
    INFO:     L    = log entry type, string (GPS|PM1|PM2.5|PT|MQ-2)
    INFO:     ID   = device ID, string ("P001")
    INFO:     UTC  = ISO 8601 date/timestamp in UTC
    INFO:   L="GPS" entries:
    INFO:     LAT  = latitude
    INFO:     LNG  = longitude
    INFO:     ALT  = altitude above mean sea level, m
    INFO:   L="PM1" and "PM2.5" entries:
    INFO:     ST   = aerosol data status (OK|SUSPECT)
    INFO:     NDC  = number of duplicate aerosol sensor clear interrupts found during sampling period
    INFO:     NDD  = number of duplicate aerosol detected interrupts found during sampling period
    INFO:     TS   = duration of sampling period, in microseconds
    INFO:     TD   = total time aerosol was detected during sampling period, in microseconds
    INFO:     R    = percent of sampling period when aerosol was dectected (100 * TD / TS)
    INFO:     C    = aerosol concentration, in particles / liter
    INFO:   L="PT" entries:
    INFO:     PATM = ambient air pressure, Pa
    INFO:     TATM = ambient air temperature, C
    INFO:     RH   = relative humidity, %
    INFO:   L="MQ-2" entries:
    INFO:     GD   = MQ-2 gas sensor digital output
    INFO:     GA   = MQ-2 gas sensor analog output
    INFO: Every log entry begins with the L, ID, and UTC fields, contains various fields determined by
    INFO: the L field value, and is terminated with '*HH' which is the cumulative XOR checksum of the
    INFO: log entry in hexadecimal. See NMEA checksum for details on calculating and validating
    INFO: the log entry checksum.
    INFO: L="GPS" entries contain L, ID, UTC, LAT, LNG, and ALT fields.
    INFO: L="PM1" and "PM2.5" entries contain L, ID, UTC, TS, ST, NDC, NDD, TD, R, and C fields.
    INFO: L="PT" entries contain L, ID, UTC, PATM, TATM, and RH fields.
    INFO: L="MQ-2" entries contain L, ID, UTC, GD, and GA fields.
    INFO: Setting up SD card filesystem.
    INFO: Mounted SD card interface.
    INFO: SD Card Type: SDHC
    INFO: SD Card Size: 30436 MB, 31914 MiB
    INFO: SD filesystem seems ok.
    INFO: SD filesystem contains:
    INFO: Listing directory: /
    INFO:  FILE: /20181101_P001_aerosniff.log  SIZE: 219997
    INFO:  FILE: /20181102_P001_aerosniff.log  SIZE: 431277
    INFO: SPIFFS filesystem formatted and operable.
    INFO: Initializing pressure/temperature sensor.
    INFO: SPIFFS filesystem seems ok.
    INFO: SPIFFS filesystem contains:
    INFO: Listing directory: /
    INFO:  FILE: /20181101_P001_aerosniff.log  SIZE: 2907
    INFO: Setting clock to GPS serial time 1541196729
    INFO: System time and date set from GPS and updated to 20181102T221209Z
    INFO: Latitude is 30.3335, Longitude is -97.7325, Altitude is 207.8 m
    INFO: Setting log file to "/20181102_P001_aerosniff.log"
    INFO: ==========================================================================
    L="GPS", ID="P001", UTC="20181102T221209Z", LAT=30.333522, LNG=-97.732542, ALT=207.8*62
    L="PM1", ID="P001", UTC="20181102T221239Z", ST="OK", NDC=0, NDD=0, TS=30000952, TD=424553, R=1.42, C=7.320E+02*1F
    L="PM2.5", ID="P001", UTC="20181102T221239Z", ST="OK", NDC=0, NDD=0, TS=30000952, TD=0, R=0, C=6.200E-01*18
    L="PT", ID="P001", UTC="20181102T221239Z", PATM=99297.48, TATM=23.78, RH=57.96*67
    L="MQ-2", ID="P001", UTC="20181102T221239Z", GD=1, GA=1190*6F
    L="PM1", ID="P001", UTC="20181102T221310Z", ST="OK", NDC=0, NDD=0, TS=30000327, TD=1425265, R=4.75, C=2.503E+03*2D
    L="PM2.5", ID="P001", UTC="20181102T221310Z", ST="OK", NDC=0, NDD=0, TS=30000327, TD=209968, R=0.7, C=3.631E+02*39
    L="PT", ID="P001", UTC="20181102T221310Z", PATM=99296.11, TATM=23.82, RH=57.87*65
    L="MQ-2", ID="P001", UTC="20181102T221310Z", GD=1, GA=1149*61
    L="PM1", ID="P001", UTC="20181102T221342Z", ST="OK", NDC=0, NDD=0, TS=30000827, TD=0, R=0, C=6.200E-01*0E
    L="PM2.5", ID="P001", UTC="20181102T221342Z", ST="OK", NDC=0, NDD=0, TS=30000827, TD=0, R=0, C=6.200E-01*16
    L="PT", ID="P001", UTC="20181102T221342Z", PATM=99291.67, TATM=23.87, RH=57.71*68
    L="MQ-2", ID="P001", UTC="20181102T221342Z", GD=1, GA=1098*6B
    L="PM1", ID="P001", UTC="20181102T221413Z", ST="OK", NDC=0, NDD=0, TS=30000715, TD=0, R=0, C=6.200E-01*03
    L="PM2.5", ID="P001", UTC="20181102T221413Z", ST="OK", NDC=0, NDD=0, TS=30000715, TD=0, R=0, C=6.200E-01*1B
    L="PT", ID="P001", UTC="20181102T221413Z", PATM=99289.86, TATM=23.9, RH=57.65*5E
    L="MQ-2", ID="P001", UTC="20181102T221413Z", GD=1, GA=1056*6A
    L="PM1", ID="P001", UTC="20181102T221444Z", ST="OK", NDC=0, NDD=0, TS=30000809, TD=472112, R=1.57, C=8.138E+02*1E
    L="PM2.5", ID="P001", UTC="20181102T221444Z", ST="OK", NDC=0, NDD=0, TS=30000809, TD=0, R=0, C=6.200E-01*1B
    L="PT", ID="P001", UTC="20181102T221444Z", PATM=99292, TATM=23.92, RH=57.55*47
    L="MQ-2", ID="P001", UTC="20181102T221444Z", GD=1, GA=1048*67

Log entries written to files are in the format of the last 21 lines (those starting with `L=`), *i.e.*:

    L="GPS", ID="P001", UTC="20181102T221209Z", LAT=30.333522, LNG=-97.732542, ALT=207.8*62
    L="PM1", ID="P001", UTC="20181102T221239Z", ST="OK", NDC=0, NDD=0, TS=30000952, TD=424553, R=1.42, C=7.320E+02*1F
    L="PM2.5", ID="P001", UTC="20181102T221239Z", ST="OK", NDC=0, NDD=0, TS=30000952, TD=0, R=0, C=6.200E-01*18
    L="PT", ID="P001", UTC="20181102T221239Z", PATM=99297.48, TATM=23.78, RH=57.96*67
    L="MQ-2", ID="P001", UTC="20181102T221239Z", GD=1, GA=1190*6F
    L="PM1", ID="P001", UTC="20181102T221310Z", ST="OK", NDC=0, NDD=0, TS=30000327, TD=1425265, R=4.75, C=2.503E+03*2D
    L="PM2.5", ID="P001", UTC="20181102T221310Z", ST="OK", NDC=0, NDD=0, TS=30000327, TD=209968, R=0.7, C=3.631E+02*39
    L="PT", ID="P001", UTC="20181102T221310Z", PATM=99296.11, TATM=23.82, RH=57.87*65
    L="MQ-2", ID="P001", UTC="20181102T221310Z", GD=1, GA=1149*61
    L="PM1", ID="P001", UTC="20181102T221342Z", ST="OK", NDC=0, NDD=0, TS=30000827, TD=0, R=0, C=6.200E-01*0E
    L="PM2.5", ID="P001", UTC="20181102T221342Z", ST="OK", NDC=0, NDD=0, TS=30000827, TD=0, R=0, C=6.200E-01*16
    L="PT", ID="P001", UTC="20181102T221342Z", PATM=99291.67, TATM=23.87, RH=57.71*68
    L="MQ-2", ID="P001", UTC="20181102T221342Z", GD=1, GA=1098*6B
    L="PM1", ID="P001", UTC="20181102T221413Z", ST="OK", NDC=0, NDD=0, TS=30000715, TD=0, R=0, C=6.200E-01*03
    L="PM2.5", ID="P001", UTC="20181102T221413Z", ST="OK", NDC=0, NDD=0, TS=30000715, TD=0, R=0, C=6.200E-01*1B
    L="PT", ID="P001", UTC="20181102T221413Z", PATM=99289.86, TATM=23.9, RH=57.65*5E
    L="MQ-2", ID="P001", UTC="20181102T221413Z", GD=1, GA=1056*6A
    L="PM1", ID="P001", UTC="20181102T221444Z", ST="OK", NDC=0, NDD=0, TS=30000809, TD=472112, R=1.57, C=8.138E+02*1E
    L="PM2.5", ID="P001", UTC="20181102T221444Z", ST="OK", NDC=0, NDD=0, TS=30000809, TD=0, R=0, C=6.200E-01*1B
    L="PT", ID="P001", UTC="20181102T221444Z", PATM=99292, TATM=23.92, RH=57.55*47
    L="MQ-2", ID="P001", UTC="20181102T221444Z", GD=1, GA=1048*67

A longer example of serial output is given in [example_serial.txt](https://bitbucket.org/apthorpe/aerosniff/src/default/userdoc/example_serial.txt).

## Open Issues ##

* None of the sensors are calibrated or have been checked against a standard.
* The PM2.5 particle concentration is using the PM1 correlation between the low pulse occupancy (LPO) ratio and concentration; it is not clear this is an appropriate correlation.
* It is not clear how to handle samples where pulses are known to have been lost. Presently, these samples are marked as `SUSPECT` in the data log and can be filtered out during post-processing if desired.
* Raw results from the MQ-2 sensor are logged. No attempt has been made to convert these readings into physical units. Also, the MQ-2 sensor appears to read higher than the maximum expected value from the sensor of 1023.
* At present, there is no way to retrieve the data files from the ESP32 filesystem or to delete them to free up space.

See [`TODO.md`](https://bitbucket.org/apthorpe/aerosniff/src/default/TODO.md) for the current list of potential enhancements to address some or all of these deficiencies.

## References ##

A number of people have worked with the Shinyei PPD42NS dust sensor in various capacities. The following resources may help 

* Hinds, William "Aerosol Technology", 2nd Edition, Wiley, 1999, ISBN-13: 9780471194101, https://books.google.com/books/about/Aerosol_technology.html?id=ORxSAAAAMAAJ
* Johnson, Nick "air\_quality" (Arduino-based particulate monitor) https://github.com/nejohnson2/air_quality
* Johnson, N. E., Bonczak, B., Kontokosta C. E. "Using a gradient boosting model to improve the performance of low-cost aerosol monitors in a dense, heterogeneous urban environment" (2018) Atmospheric Environment 184, 9-16 https://www.sciencedirect.com/science/article/pii/S1352231018302504?via%3Dihub
* Allen, Tracy "De-construction of the Shinyei PPD42NS dust sensor", version 0.2, 30-May-2013 http://takingspace.org/wp-content/uploads/ShinyeiPPD42NS_Deconstruction_TracyAllen.pdf
* Austin, Elena, Novosolev, Igor, Seto, Edmund, Yost, Michael, "Laboratory Evaluation of the Shinyei PPD42NS Low-Cost Particulate Matter Sensor", 2015 https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0137789 
* "Grove - Dust Sensor User Manual" https://www.mouser.com/ds/2/744/Seeed_101020012-1217636.pdf
* "Shinyei PPD42NS Datasheet" https://github.com/SeeedDocument/Grove_Dust_Sensor/raw/master/resource/Grove_-_Dust_sensor.pdf
* indiaairquality (blog), "Measuring the Pickle Jr. – a modified PPD42 with an attached fan." https://indiaairquality.com/2014/12/14/measuring-the-pickle-jr-a-modified-ppd42-with-an-attached-fan/
* Tan, Darell, `irq5.io` blog, "Testing the Shinyei PPD42NS" http://irq5.io/2013/07/24/testing-the-shinyei-ppd42ns/
* Nafis, Chris "Air Quality Monitoring" http://www.howmuchsnow.com/arduino/airquality/grovedust/
* "The Shinyei Experiment" https://aqicn.org/sensor/shinyei/
* Johnson, Karoline K., Bergin, Michael H., Russell, Armistead G., Hagler, Gayle S. W.  "Field Test of Several Low-Cost Particulate Matter Sensors in High and Low Concentration Urban Environments" Aerosol and Air Quality Research, 18: 565–578, 2018  http://www.aaqr.org/files/article/6638/1_AAQR-17-10-OA-0418_565-578.PDF
* Price, Thomas "Air Quality Monitor", https://cdn.hackaday.io/files/21912937483008/Thomas_Portable_Air_Quality.pdf via https://hackaday.io/project/21912-portable-air-quality-monitor April 2017
* Holstius, D. M., Pillarisetti, A., Smith, K. R., Seto, E. "Field calibrations of a low-cost aerosol sensor at a regulatory monitoring site in California" Atmos. Meas. Tech., 7, 1121-1131, 2014 https://doi.org/10.5194/amt-7-1121-2014 with supplement https://www.atmos-meas-tech.net/7/1121/2014/amt-7-1121-2014-supplement.pdf

"Aerosol Technology" by Hinds is a very good technical resource on the properties and behavior of aerosols as well as on practical monitoring and experimental techniques.

## Disclaimer ##

This software is provided as an educational resource and no warranty is made to its accuracy or suitability for engineering use, especially in any use involving protection of human life and environmental quality. *Caveat utilitor!*

## License ##

All code provided under the MIT (`expat`) license as written in [`LICENSE.md`](https://bitbucket.org/apthorpe/aerosniff/src/default/LICENSE.md)

## Who do I talk to? ##

Questions or comments can be directed to the project maintainer, <bob.apthorpe@gmail.com>
