# A Brief List of Potential Enhancements #

Aerosniff is currently built as a benchtop demonstrator from a breadboard, an ESP32 development board, and several sensors mounted on breakout boards. It is by no means a deployable prototype, let alone a deliverable product. Several enhancements such as those listed below would make Aerosniff more useful as a research tool.

## Create a Reference Log Processing Utility ##

Log entries currently require post-processing to easily plot or analyze yet there is no reference implementation for a log processing utility.

**Note:** The Python library `aniso8601` (and many more) can directly convert ISO 8601 timestamps to `datetime` objects. This should allow log entries to be easily imported into a Pandas dataframe or equivalent.

## DONE: Make Sensors Optional ##

**Status**: `#define`s have been added to the code to selectively enable/disable sensors. All sensors may be toggled as present or absent. The PM1, PM2.5, MQ-2 (combustible gas) and BME280 (pressure, temperature, relative humidity) sensors have been tested along with the GPS, SPIFFS, and SD card peripherals.

Since PM2.5 particle monitoring requires a hardware modification (adding an additional line to the PPD42's JST XH connector), some users may want to use only the PM1 sensor line. Likewise, the MQ-2 gas sensor may not be relevant for some users. At bare minimum, Aerosniff should support PM1 aerosol monitoring, barometric pressure, ambient air temperature, and accurate time (e.g. from NTP, GPS, or a RTC). Other sensors should be optional. This has implications for logging as noted below.

## Add SD Card Support ##

**Status**: Basic SD card support has been added and logs are written to the SD card by default.

The flash file system on the ESP32 is not large enough to store more than a few days of logs under the defaut settings. Also, writing and rewriting flash memory is destructive and there is no way to replace or recondition the ESP32's onboard flash memory; the entire ESP32 module must be replaced. Finally, there is no way to retrieve logs from the ESP32's flash memory without the ESP32 module being operable; if the processor fails, logs are irrevocably lost. To solve these issues, SD card support should be added. This allows for flexible, removable, replaceable storage with sufficient capacity to hold many days worth of logs. Further, this frees up the ESP32's flash memory for use as program space or for over-the-air (OTA) upgrades.

## DONE: Change Log Entry Format to Sentence Model ##

**Note**: This change has been implemented and the following text has been altered slightly from the original to be consistent with the as-implemented behavior.

The log entry is monolithic - all data taken is stored on a single uniform line. What may be more appropriate is a 'sentence'-like approach as taken with NMEA (i.e. GPS) data. Define a per-sensor log entry format and prefix the log entry with the sensor type. For example, the current log entry format might change from:

    ID="P001", LAT=30.333498, LNG=-97.732502, ALT=188.1, UTC="20181025T145544Z", TS=30000263, ST1="OK", NDC1=0, NDD1=0, TD1=5427602, R1=18.1, C1=1.468E+04, ST2="OK", NDC2=0, NDD2=0, TD2=1260785, R2=4.2, C2=2.200E+03, PATM=99297.61, TATM=22.51, RH=62.02, GD=1, GA=1501*5F

to:

    L="GPS", ID="P001", UTC="20181025T145544Z", LAT=30.333498, LNG=-97.732502, ALT=188.1*00
    L="PT", ID="P001", UTC="20181025T145544Z", PATM=99297.61, TATM=22.51, RH=62.02*00
    L="PM1", ID="P001", UTC="20181025T145544Z", ST="OK", NDC=0, NDD=0, TS=30000263, TD=5427602, R=18.1, C=1.468E+04*00
    L="PM2.5", ID="P001", UTC="20181025T145544Z", ST="OK", NDC=0, NDD=0, TS=30000263, TD=1260785, R=4.2, C=2.200E+03*00
    L="MQ-2", ID="P001", UTC="20181025T145544Z", GD=1, GA=1501*00

This complicates log processing since the format isn't easily imported as uniform CSV however the log format was designed to allow trivial post-processing to produce JSON or CSV format, but was intentionally designed to **not** be trivially compatible with CSV.

Given the data are already heterogenous and there may be a desire to support more and different sensors on the same data acquisition platform, changing the log format from a uniform monolithic structure to a heterogenous `sentence` structure makes good sense.

*Aside:* The expectation in the original design was that raw sensor logs would be post-processed to verify checksums, filter out suspect data points, perform trivial conversions (*e.g.* convert UTC timestamp to local or serial time), and to write the processed data to a standard format for further analysis, plotting, *etc.* The expectation was also that if the log was in CSV format, users would directly import it into a spreadsheet for plotting and data analysis which is problematic from an audit and peer review standpoint. Spreadsheets are notoriously difficult to validate and review compared to computer programs so in the interest of good science and engineering, the log format was designed to require auditable post-processing, however trivial. A motivated user could still import the raw logs into a spreadsheet for analysis but it would not be as easy as post-processing the logs in a traceable, auditable manner. This seems parochial, but long experience with using spreadsheets in an environment where peer review is part of the workflow has biased the author against the use of spreadsheets for anything but plotting. And even then.

## Add Log File Retrieval Capability ##

**Status**: SD card support has been added and logs are written to the SD card by default. This allows logs to be retrieved from the physical device.

At present, there is no way to retrieve log data from the device. Serial commands could be used to dump log entries to the console where they could be recorded; this is maybe the easiest method but requires physical access to the device and negates the value of using a radio-equipped MCU like the ESP32.

HTTP, FTP, or SSH/SCP access to the device would be better, allowing the data to be pulled from the device over wifi, potentially remotely. This would necessitate putting the device on the internet or an intranet which comes with a set of security risks. From a security standpoint, a 'pull' model is preferable since it requires no server security credentials to be stored on the device. Since the dust monitor is expected to operate unattended in a remote location, physical security cannot be guaranteed and it is irresponsible to leave server credentials on a device which cannot be secured. With a 'pull' model, even if the device was compromised, no other devices or servers would be affected.

Bluetooth file access would also be acceptable. From a security standpoint, it is likely more secure (less abuseable) than wifi but the short range of Bluetooth means data would need to be collected locally from the device in the field. It is slightly more convenient than needed serial console access but not much more. A Bluetooth to wifi or GPRS gateway device may be a reasonable approach if multiple sensors are within Bluetooth range but this adds complexity and cost to data acquisition, negating the project goal of making mass remote sensing and data acquisition affordable.

Transmitting data over GPRS may be the best option due to the range and coverage of the cellular network. This negates the value of using the ESP32 and increases cost, requiring a SIM card and cell service for each sensor. However, the tradeoff in accessibility and security may be worth it. This might necessitate changing to an MCU other than the ESP32, one with more processing power than the 8-bit AVR MCUs, possibly an ARM core device such as the STM32-series MCU or Arduino DUE. Depending on the cost of GPRS modules which support the ESP32, it may make sense just to add an external module and use Bluetooth and wifi for provisioning and local configuration of the Aerosniff.

## Design a Power System for Remote Use ##

No thought has gone into powering the device as a remote sensor so consequently, no effort as yet has gone toward measuring or reducing power requirements. The use of GPS and the heater resistor in the aerosol sensor imply the device will need substantial power and the need for the dust sensor heater to be at temperature before a reading is taken makes putting the device into deep sleep mode untenable unless the time between aerosol samples is on the order of hours not minutes.

Solar panels with a LiPo charger is the first thought, though again, power requirements need to be estimated to see if this is practical and to estimate solar panel and battery capacity. Another approach would be to use a large lead-acid battery such as those used in an uninterruptible power supply (UPS). They are affordable if not cheap, they have a substantial capacity, they are standard and available in a variety of capacities, and they can be recharged. They are also large and heavy.

Power systems designed for similar remote sensing units should be investigated; it is better to use an established and reliable power system design rather than spend the effort inventing one from scratch.

## Design a Hardware Enclosure ##

For field testing, the sensors, battery, and MCU need to be protected from the elements - principally water, but also insects and animals, heat, cold, humidity, and vandals. Again, reviewing how other similar sensors have been designed to withstand environmental challenges is necessary. For initial testing, 3D printing and repurposing of existing enclosures ("yogurt containers and gaffer tape") is probably adequate.

## Design a Custom PCB ##

For mass deployment, it may be cost effective to migrate from common off-the-shelf (COTS) components to a bespoke device based on a custom printed circuit board (PCB). Custom PCBs fabrication services are available at low cost which makes developing custom hardware an option. However, the time and reliability cost of populating a board with components may negate any value of using custom hardware vs assembling the Aerosniff from development boards and sensor breakout boards.

## Have Someone Experienced Review the Hardware and Code ##

Once the log data retrieval/export subsytem has been implemented and the hardware has advanced to a prototype stage (power, enclosure, and possibly custom mainboard), the system should be reviewed by experienced hardware and firmware engineers to suggest improvements. Design-for-manufacturability may not be in scope since the prototype is composed of a development board wired to breakout boards, however the choice of hardware and toolchain as well as all aspects of firmware design & implementation should be evaluated. Of special importance is security since IoT device security is often less than an afterthought.

